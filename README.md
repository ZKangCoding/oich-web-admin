# oich-web-admin

## Project setup

```
npm install
```

**Compiles and hot-reloads for development**

```
npm run serve
```

**Compiles and minifies for production**

```
npm run build
```



## 介绍

非遗项目后台界面



## 技术栈

- Vue 3 + Ant Design Vue
